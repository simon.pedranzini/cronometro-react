import { CuentaAtras, RelojDigital } from './components';

import './App.scss';

function App() {
  return (
    <div className="app">
<h1 className="title-app">El tiempo es el recurso más valioso que tenemos</h1>
    <div>
      <RelojDigital />
    </div>
    <div>
      <CuentaAtras />
    </div>
    </div>
  );
}

export default App;
