import React, { useEffect, useState } from 'react'

import './CuentaAtras.scss'

const CuentaAtras = () => {

  const [time, setTime] = useState('');

  useEffect(() => {
    let countDownDate = new Date("Feb 11, 2022 22:44:00").getTime();
    let x = setInterval(() => {
      let now = new Date().getTime();
          let distance = countDownDate - now;
          let days = Math.floor(distance / (1000 * 60 * 60 * 24));
          let hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
          let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
          let seconds = Math.floor((distance % (1000 * 60)) / 1000);

          setTime(days + "d " + hours + "h " + minutes + "m " + seconds + "s ");

          if (distance < 0) {
            clearInterval(x);
            setTime("CUENTA ATRÁS FINALIZADA");
          }
    }, 1000);
  }, [])

  return (
    <div>
      <span className="cuenta-atras-title">CuentaAtras</span>
      <p>{time}</p>
      </div>
    )
}

export default CuentaAtras