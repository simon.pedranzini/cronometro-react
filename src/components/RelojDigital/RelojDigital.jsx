import React, { useEffect, useState } from 'react'

import './RelojDigital.scss'

const RelojDigital = () => {
    const [clockState, setClockState] = useState();

    useEffect(() => {
        setInterval(() => {
            const date = new Date();
            setClockState(date.toLocaleTimeString());
        }, 1000);
    }, []);

  return (
    <div className='clock-general'>
      <span className='clock-title'>Reloj digital</span>
        <p className='clock-time'>{clockState}</p>
    </div>
  )
}

export default RelojDigital